if(!/production/i.test(process.env.NODE_ENV)){
  require('require-dynamic-exec').watch(__dirname,true)
}
require('.').server.listen(
  process.env.npm_package_config_port,
  function(){
    console.log(`server is running on ${this.address().port}`)
  }
)