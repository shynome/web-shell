FROM node:carbon-alpine
ENV NODE_ENV=production

WORKDIR /app
COPY . /app
RUN npm i

EXPOSE 80 443

CMD npm start
