module.exports =  require('express').Router()

.get('*.html.js',(req,res)=>res.status(403).send('403 Forbidden'))
.use('/',require('express').static(__dirname+'/../views/',{}))

.get(['/','/index.*'],(req,res)=>{
  res.send(require('../views/index.html')())
})
