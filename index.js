'use strict'

exports.app = require('./app').app

exports.server = new (require('http').Server)(exports.app)

exports.io = require('socket.io')(exports.server)
